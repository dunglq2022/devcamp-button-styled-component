import styled from 'styled-components'

// export default styled.button `
//     background-color: #7b4cd8;
//     color: white;
//     border-radius: 5px;
//     padding: 10px;
//     border: none;
//     font-size: 16px;
// `

export default styled.button({
    backgroundColor: '#7b4cd8',
    color: 'white',
    borderRadius: '5px',
    padding: '10px',
    border: 'none',
    fontSize: '16px'
});
