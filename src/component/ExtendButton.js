import styled from 'styled-components'

export const SuperButton = styled.button `
    background-color: #cacaca;
    color: white;
    border-radius: 5px;
    padding: 10px;
    margin: 6px;
    border: none;
    font-size: 16px;
`;
export const ChildButton = styled(SuperButton)`
    background-color: ${props => props.bgColor || 'red'};
    color: ${props => props.fontColor || 'white'};
`;