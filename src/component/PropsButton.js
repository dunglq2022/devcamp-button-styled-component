import styled from 'styled-components'

const PropButton = styled.button `
    background-color: ${props => props.bgColor || 'red'};
    color: ${props => props.fontColor || 'white'};
    border-radius: 5px;
    padding: 10px;
    margin: 6px;
    border: none;
    font-size: 16px;
`

const PrimaryButton = styled.button `
    background-color: ${props => props.primary ? '#7b4cd8' : '#ff31ca'};
    color: ${props => props.primary ? 'white' : '#ff7cdc'};
    border-radius: 5px;
    padding: 10px;
    margin: 6px;
    border: none;
    font-size: 16px;
`

export {PropButton, PrimaryButton};