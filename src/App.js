import './App.css';
import {SuperButton, ChildButton} from './component/ExtendButton';
import {PropButton, PrimaryButton} from './component/PropsButton';

import StyledButton from './component/StyledButton';

function App() {
  return (
    <div >
      <h4>1. Tạo style button theo cách styled component</h4>
      <StyledButton>I am a styled button!</StyledButton>
      <h4>2. Tạo style button truyền vào màu nền</h4>
      <PropButton bgColor='#ffc3c3' fontColor='#d0533a'>Button 1</PropButton>
      <PropButton  bgColor='#ffdaca' fontColor='#e38963'>Button 2</PropButton>
      <PropButton bgColor='#fff4c7' fontColor='#d0b459'>Button 3</PropButton>
      <PropButton>Button 4</PropButton>
      <h4>3. Tạo style button nếu có props "primảy" thì hiển thị màu khác</h4>
      <PrimaryButton primary>Primary Button</PrimaryButton>
      <PrimaryButton>Other Button</PrimaryButton>
      <PrimaryButton>Other Button</PrimaryButton>
      <h4>4. Tạo style button Super và button con có màu nền và màu chữ khác</h4>
      <SuperButton>Super Button</SuperButton>
      <ChildButton bgColor='#ff1493'>Hello!1</ChildButton>
      <ChildButton bgColor='#ff69b4'>Hello!2</ChildButton>
      <ChildButton bgColor='#ff7f50'>Hello!3</ChildButton>      
    </div>
  );
}

export default App;
